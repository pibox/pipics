/*******************************************************************************
 * pipics
 *
 * db.h:  Functions for playing images in sequence
 *
 * License: GPL v3
 *
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#ifndef DB_H
#define DB_H

/* 
 * ========================================================================
 * Type definitions and Data structures 
 * =======================================================================
 */

/*
 * ========================================================================
 * Defined values, some of which are used in test modes only
 * =======================================================================
 */

/* Production locations for local media on USB sticks */
#define DBTOP       "/media/usb"

/* Test location for local media */
#define DBTOP_T     "data"

/*
 * ========================================================================
 * Prototypes
 * =======================================================================
 */
#ifndef DB_C
extern void     dbLoad( void );
extern char     *getActiveFile( void );
extern void     getNextImage( void );
extern void     getPrevImage( void );
#endif

#endif /* DB_H */
