/*******************************************************************************
 * pipics
 *
 * pipics.h:  program main
 *
 * License: GPL v3
 *
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#ifndef PIPICS_H
#define PIPICS_H

#include <gtk/gtk.h>

#define PROG        "pipics"

#define KEYSYMS_F       "/etc/pibox-keysyms"
#define KEYSYMS_FD      "data/pibox-keysyms"
#define PIPICS_DATA_F   "/tmp/pipics.data"

/*========================================================================
 * Globals
 *=======================================================================*/
#ifdef PIPICS_C
int     daemonEnabled = 0;
char    errBuf[256];
#else
extern int      daemonEnabled;
extern char     errBuf[];
#endif /* PIPICS_C */

/*========================================================================
 * Defined values
 *=======================================================================*/
#define PROG        "pipics"
#define MAXBUF      4096

/* Where the config file is located */
#define F_CFG           "/etc/pipics.cfg"
#define F_CFG_T         "data/pipics.cfg"
#define F_DISPLAYCFG_T  "data/pibox-config"
#define F_DISPLAYCFG    "/etc/pibox-config"

/*========================================================================
 * Prototypes
 *=======================================================================*/
#ifndef PIPICS_C
extern void     imageTouch( int region );
extern void     do_drawing();
extern guint    getChoice( void );
extern gboolean startKiosk (gpointer data);
extern gboolean endKiosk (gpointer data);
extern gboolean displayImage (gpointer data);
extern void     playImages( void );
#endif

/*========================================================================
 * Include other headers
 *=======================================================================*/
#include <pibox/log.h>
#include "db.h"
#include "cli.h"
#include "utils.h"

#endif /* !PIPICS_H */

