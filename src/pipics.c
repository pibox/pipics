/*******************************************************************************
 * pipics
 *
 * pipics.c:  program main
 *
 * License: GPL v3
 *
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#define PIPICS_C

/* To avoid deprecation warnings from GTK+ 2.x */
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"

#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <cairo.h>
#include <gtk/gtk.h>
#include <gdk/gdkkeysyms.h>
#include <pibox/pibox.h>

#include "pipics.h"

/* Local prototypes */
static gboolean key_press(GtkWidget *widget, GdkEventKey *event, gpointer user_data);
gboolean imageTouchGTK( int region );
gboolean starKiosk (gpointer data);
gboolean endKiosk (gpointer data);
guint do_drawing();
void playImages();

GtkWidget       *window;
GtkWidget       *darea;
cairo_surface_t *image = NULL;
cairo_t         *cr = NULL;
guint           image_cycle_id = 0;
int             inprogress = 0;

// The home key is the key that exits the app.
guint homeKey = -1;

/*
 *========================================================================
 * Name:   pipicsExit
 * Prototype:  void pipicsExit( void )
 *
 * Description:
 * Graceful exit of the app.
 *========================================================================
 */
void
pipicsExit( void )
{
    gtk_main_quit();
}

/*
 *========================================================================
 * Name:   pipicsNext
 * Prototype:  void pipicsNext( void )
 *
 * Description:
 * Graceful exit of the app.
 *========================================================================
 */
void
pipicsNext( void )
{
    piboxLogger(LOG_INFO, "Getting next image.\n");
    if ( image_cycle_id )
    {
        piboxLogger(LOG_INFO, "Removing timer\n");
        g_source_remove(image_cycle_id);
        image_cycle_id = 0;
    }
    getNextImage();
    playImages();
}

/*
 *========================================================================
 * Name:   pipicsPrev
 * Prototype:  void pipicsPrev( void )
 *
 * Description:
 * Graceful exit of the app.
 *========================================================================
 */
void
pipicsPrev( void )
{
    piboxLogger(LOG_INFO, "Getting prev image.\n");
    if ( image_cycle_id )
    {
        piboxLogger(LOG_INFO, "Removing timer\n");
        g_source_remove(image_cycle_id);
        image_cycle_id = 0;
    }
    getPrevImage();
    playImages();
}

/*
 *========================================================================
 * Name:   imageTouch
 * Prototype:  void imageTouch( int region )
 *
 * Description:
 * Handler for touch region reports.  It handles moving to next/prev
 * images and quitting the app.
 *
 *   --------------------------------
 *   | 0: N/A | 1: N/A    | 2: Quit | 
 *   --------------------------------
 *   | 3: Prev| 4: N/A    | 5: Next |
 *   --------------------------------
 *   | 6: N/A | 7: N/A    | 8: N/A  |
 *   --------------------------------
 *
 * Notes:
 * We use "killall" here to avoid having to track process ids.  It's just
 * easier that way.
 *========================================================================
 */
void
imageTouch( int region )
{
    g_idle_add( (GSourceFunc)imageTouchGTK, (gpointer)region );
}

gboolean
imageTouchGTK( int region )
{
    /* Prevent multiple touches from being handled at the same time. */
    if ( inprogress )
    {
        piboxLogger(LOG_INFO, "In progress, skipping update\n");
        return FALSE;
    }
    inprogress = 1;

    switch( region )
    {
        /* Quit */
        case 2:
            piboxLogger(LOG_INFO, "Quit region\n");
            pipicsExit();
            break;

        /* Prev image */
        case 3:
            piboxLogger(LOG_INFO, "Prev image region\n");
            pipicsPrev();
            break;

        /* Next image */
        case 5:
            piboxLogger(LOG_INFO, "Next image region\n");
            pipicsNext();
            break;

        default:
            piboxLogger(LOG_ERROR, "Invalid region: %d\n", region);
            break;
    }
    return(FALSE);
}

/*
 *========================================================================
 * Name:   loadKeysyms
 * Prototype:  void loadKeysyms( void )
 *
 * Description:
 * Read in the keysym file so we know how the platform wants us to behave.
 *
 * Notes:
 * Format is KEYSYM NAME:ACTION
 *========================================================================
 */
void
loadKeysyms( void )
{
    char        *tsave = NULL;
    struct stat stat_buf;
    char        *keysym;
    char        *action;
    char        *ptr;
    FILE        *fd;
    char        buf[128];
    char        *path;

    if ( isCLIFlagSet( CLI_TEST) )
        path = KEYSYMS_FD;
    else
        path = KEYSYMS_F;

    // Read in /etc/pibox-keysysm
    if ( stat(path, &stat_buf) != 0 )
    {
        piboxLogger(LOG_INFO, "No keysym file: %s\n", path);
        return;
    }

    fd = fopen(path, "r");
    if ( fd == NULL )
    {
        piboxLogger(LOG_ERROR, "Failed to open keysyms file: %s - %s\n", path, strerror(errno));
        return;
    }

    memset(buf, 0, 128);
    while( fgets(buf, 127, fd) != NULL )
    {
        // Ignore comments
        if ( buf[0] == '#' )
            continue;

        // Strip leading white space
        ptr = buf;
        ptr = piboxTrim(ptr);

        // Ignore blank lines
        if ( strlen(ptr) == 0 )
            continue;

        // Strip newline
        piboxStripNewline(ptr);

        // Grab first token
        keysym = strtok_r(ptr, ":", &tsave);
        if ( keysym == NULL )
            continue;

        // Grab second token
        action = strtok_r(NULL, ":", &tsave);
        if ( action == NULL )
            continue;

        piboxLogger(LOG_INFO, "keysym / action: %s / %s \n", keysym, action);

        // Set the home key
        if ( strncasecmp("home", action, 4) == 0 )
        {
            homeKey = gdk_keyval_from_name(keysym);
            piboxLogger(LOG_INFO, "homeKey = %08x\n", homeKey);
        }
    }
}

/*
 *========================================================================
 * Name:   loadDisplayConfig
 * Prototype:  void loadDisplayConfig( void )
 *
 * Description:
 * Read in the display config file so we know how the display should be 
 * handled.
 *
 * Notes:
 * Format is 
 *     DISPLAY TYPE (DVT LCD)
 *     RESOLUTION
 *========================================================================
 */
void
loadDisplayConfig( void )
{
    int width;
    int height;

    piboxLoadDisplayConfig();
    width = piboxGetDisplayWidth();
    height = piboxGetDisplayHeight();
    if ( (width<=800) || (height<=480) )
    {
        setCLIFlag( CLI_SMALL_SCREEN );
    }

    if ( piboxGetDisplayType() == PIBOX_LCD )
    {
        setCLIFlag(CLI_TOUCH);
    }
    else
    {
        unsetCLIFlag(CLI_TOUCH);
    }
}

/*
 *========================================================================
 * Name:   key_press
 * Prototype:  void key_press( GtkWidget *, GdkEventExpose *, gpointer )
 *
 * Description:
 * Handles exiting the application via keystrokes.
 *========================================================================
 */
static gboolean
key_press(GtkWidget *widget, GdkEventKey *event, gpointer user_data)
{
    switch(event->keyval)
    {
        case GDK_KEY_Q:
        case GDK_KEY_q:
            if (event->state & GDK_CONTROL_MASK)
            {
                piboxLogger(LOG_INFO, "Ctrl-Q key\n");
                pipicsExit();
                return(TRUE);
            }
            break;

        case GDK_KEY_Home:
            piboxLogger(LOG_INFO, "Home key\n");
            pipicsExit();
            return(TRUE);
            break;

        case GDK_KEY_Left:
        case GDK_KEY_KP_Left:
            piboxLogger(LOG_INFO, "Prev image\n");
            pipicsPrev();
            return(TRUE);
            break;

        case GDK_KEY_Right:
        case GDK_KEY_KP_Right:
            piboxLogger(LOG_INFO, "Next image\n");
            pipicsNext();
            return(TRUE);
            break;

        case GDK_KEY_space:
        case GDK_KEY_Return:
        case GDK_KEY_KP_Space:
        case GDK_KEY_KP_Enter:
            piboxLogger(LOG_INFO, "Next image\n");
            pipicsNext();
            return(TRUE);
            break;

        default:
            if ( event->keyval == homeKey )
            {
                piboxLogger(LOG_INFO, "Keysym configured home key\n");
                gtk_main_quit();
                return(TRUE);
            }
            else
            {
                piboxLogger(LOG_INFO, "Unknown keysym: %s\n", gdk_keyval_name(event->keyval));
                return(FALSE);
            }
            break;
    }
    return(FALSE);
}

#if 0
/*
 * ========================================================================
 * Name:   sigHandler
 * Prototype:  static void sigHandler( int sig, siginfo_t *si, void *uc )
 *
 * Description:
 * Handles program signals.
 *
 * Input Arguments:
 * int       sig    Signal that cause callback to be called
 * siginfo_t *si    Structure that includes timer over run information and timer ID
 * void      *uc    Unused but required with sigaction handlers.
 * ========================================================================
 */
static void
sigHandler( int sig, siginfo_t *si, void *uc )
{
    switch (sig)
    {
        case SIGHUP:
            /* Reset the application. */
            endKiosk;
            break;

        case SIGTERM:
            /* Bring it down gracefully. */
            endKiosk;
            break;
    }
}
#endif

/*
 *========================================================================
 * Name:   displayImage
 * Prototype:  gboolean displayImage( gpointer )
 *
 * Description:
 * Called once to start image playback.
 *========================================================================
 */
gboolean displayImage (gpointer data)
{
    /* Move to the next image to display */
    getNextImage();

    do_drawing();
    return TRUE;
}

/*
 *========================================================================
 * Name:   playImages
 * Prototype:  void playImages( void )
 *
 * Description:
 * Play the list of image files
 *========================================================================
 */
void
playImages ()
{
    /* Clear existing timer, if any. */
    if ( image_cycle_id )
    {
        piboxLogger(LOG_INFO, "Removing timer\n");
        g_source_remove(image_cycle_id);
        image_cycle_id = 0;
    }

    /* Draw current image */
    piboxLogger(LOG_INFO, "Drawing current image\n");
    while ( do_drawing() == 1 )
        getNextImage();

    /* Schedules updates every 10 seconds. */
    piboxLogger(LOG_INFO, "Starting timer.\n");
    image_cycle_id = g_timeout_add(10000, displayImage, NULL);
}

/*
 *========================================================================
 * Name:   startKiosk
 * Prototype:  gboolean startKiosk( gpointer )
 *
 * Description:
 * Called once to start image playback.
 *========================================================================
 */
gboolean startKiosk (gpointer data)
{
    playImages();
    return FALSE;
}

/*
 *========================================================================
 * Name:   endKiosk
 * Prototype:  gboolean endKiosk( gpointer )
 *
 * Description:
 * Called once to exit kiosk mode.
 *========================================================================
 */
gboolean endKiosk (gpointer data)
{
    gtk_main_quit();
    return FALSE;
}

/*
 *========================================================================
 * Name:   do_drawing
 * Prototype:  void do_drawing( void )
 *
 * Description:
 * Updates the poster area of the display.
 * 
 * Returns:
 * 0 on success
 * 1 if current image can't be found.
 *========================================================================
 */
guint
do_drawing()
{
    struct stat             stat_buf;
    gint                    iwidth, iheight;
    gint                    swidth, sheight;
    GtkRequisition          req;
    char                    *imagePath;
    GdkPixbuf               *image;
    GdkPixbuf               *newimage;
    gfloat                  ri,rs;
    double                  offset_x, offset_y;

    piboxLogger(LOG_INFO, "Entered\n");

    /* Get the active file to display */
    imagePath = getActiveFile();

    /* If poster doesn't exist skip it and go to the next one. */
    if ( (imagePath == NULL) || (stat(imagePath, &stat_buf) != 0) )
    {
        if ( imagePath != NULL )
            piboxLogger(LOG_ERROR, "imagePath doesn't exist: %s\n", imagePath);
        else 
            piboxLogger(LOG_ERROR, "imagePath is NULL.\n");
        return(1);
    }
    piboxLogger(LOG_INFO, "Trying to display file: %s\n", imagePath);

    cr = gdk_cairo_create(darea->window);
    req.width = gdk_window_get_width(darea->window);
    req.height = gdk_window_get_height(darea->window);
    piboxLogger(LOG_INFO, "window w/h: %d / %d\n", req.width, req.height);

    // Fill with white background
    cairo_set_source_rgb(cr, 0.0, 0.0, 0.0);
    cairo_set_line_width(cr, 1);
    cairo_rectangle(cr, 0, 0, req.width, req.height);
    cairo_stroke_preserve(cr);
    cairo_fill(cr);

    piboxLogger(LOG_INFO, "Creating pixbuf from file\n");
    image  = gdk_pixbuf_new_from_file(imagePath, NULL);
    newimage = gdk_pixbuf_apply_embedded_orientation (image);
    g_object_unref(image);
    image = newimage;
    iwidth  = gdk_pixbuf_get_width(image);
    iheight = gdk_pixbuf_get_height(image);

    /* Compute scaling to keep aspect ratio but fit in the screen */
    ri = iwidth / iheight;
    rs = req.width / req.height;
    if ( rs > ri )
    {
        swidth = (gint)(iwidth * req.height/iheight);
        sheight = req.height;
    }
    else
    {
        swidth = req.width;
        sheight = (gint)(iheight * req.width/iwidth);
    }

    newimage = gdk_pixbuf_scale_simple(image, swidth, sheight, GDK_INTERP_BILINEAR);
    iwidth  = gdk_pixbuf_get_width(newimage);
    iheight = gdk_pixbuf_get_height(newimage);
    piboxLogger(LOG_INFO, "image w/h: %d / %d\n", iwidth, iheight);

    /* Center on the display */
    offset_x = (double) (((double)req.width - (double)iwidth) / (double)2.0);
    offset_y = (double) (((double)req.height - (double)iheight) / (double)2.0);
    gdk_cairo_set_source_pixbuf(cr, newimage, offset_x, offset_y);
    cairo_paint(cr);
    cairo_destroy(cr);

    g_object_unref(image);
    g_object_unref(newimage);

    /* Allow more touches */
    inprogress = 0;

    return(0);
}

/*
 *========================================================================
 * Name:   createWindow
 * Prototype:  GtkWidget *createWindow( void )
 *
 * Description:
 * Creates the main window with a list of videos on the left and currently
 * selected video poster on the right.  The poster area is sensitive to
 * clicks that will start playing the video.
 *========================================================================
 */
GtkWidget *
createWindow()
{
    GtkWidget           *vbox;

    window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
    GTK_WIDGET_SET_FLAGS(window, GTK_CAN_FOCUS );
    gtk_widget_add_events(window, GDK_KEY_PRESS_MASK);
    g_signal_connect(G_OBJECT(window),
                "key_press_event",
                G_CALLBACK(key_press),
                NULL);

    // Two rows: menu and content
    vbox = gtk_vbox_new (FALSE, 0);
    gtk_widget_set_name (vbox, "vbox");
    gtk_widget_show (vbox);
    gtk_container_add (GTK_CONTAINER (window), vbox);

    /*
     * The poster area
     */
    darea = gtk_drawing_area_new();
    gtk_widget_set_size_request( darea, 600, 800);
    gtk_box_pack_start (GTK_BOX (vbox), darea, TRUE, TRUE, 0);

    /* Make the main window die when destroy is issued. */
    g_signal_connect(window, "destroy", G_CALLBACK (gtk_main_quit), NULL);

    /* Now position the window and set its title */
    gtk_window_set_position(GTK_WINDOW(window), GTK_WIN_POS_CENTER);
    gtk_window_set_default_size(GTK_WINDOW(window), 2048, 1024);
    gtk_window_set_title(GTK_WINDOW(window), "pipics");

    return window;
}

/*
 * ========================================================================
 * Name:   main
 *
 * Description:
 * Program startup
 * ========================================================================
 */
int
main(int argc, char *argv[])
{
    GtkWidget   *window;
    char        cwd[512];
    char        gtkrc[1024];

    struct stat stat_buf;
    char path[MAXBUF];

    // Load saved configuration and parse command line
    initConfig();
    parseArgs(argc, argv);
    validateConfig();

    // Setup logging
    piboxLoggerInit(cliOptions.logFile);
    piboxLoggerVerbosity(cliOptions.verbose);
    printf("Verbosity level: %d\n", piboxLoggerGetVerbosity());

    if ( cliOptions.logFile != NULL )
    {
        piboxLogger(LOG_INFO, "Log file: %s\n", cliOptions.logFile);
    }
    else
    {
        piboxLogger(LOG_INFO, "No log file configured.\n");
    }

    // Read environment config for keyboard behaviour
    loadKeysyms();

    /* Get display config information */
    loadDisplayConfig();
    piboxLogger(LOG_INFO, "display type: %s\n", cliOptions.display_type);
    piboxLogger(LOG_INFO, "display resolution: %s\n", cliOptions.display_resolution);

    // Set the path to the top of the image files.
    memset(path, 0, MAXBUF);
    if ( cliOptions.dbTop == NULL )
    {
        piboxLogger(LOG_INFO, "dbtop is null\n");
        if ( isCLIFlagSet( CLI_TEST) )
            sprintf(path, "%s", DBTOP_T);
        else
            sprintf(path, "%s", DBTOP);
    }
    else
    {
        piboxLogger(LOG_INFO, "dbtop is not null\n");
        sprintf(path, "%s", cliOptions.dbTop);
    }

    // Test for db existance.
    if ( stat(path, &stat_buf) != 0 )
    {
        piboxLogger(LOG_ERROR, "Can't find database directory");
        return(-1);
    }
    piboxLogger(LOG_INFO, "Reading database directory: %s\n", path);

    // Change to that directory
    memset(cwd, 0, 512);
    getcwd(cwd, 512);
    sprintf(gtkrc, "%s/data/gtkrc", cwd);
    piboxLogger(LOG_INFO, "gtkrc: %s\n", gtkrc);
    chdir(path);

    piboxLogger(LOG_INFO, "Running from: %s\n", getcwd(path, MAXBUF));

    /* 
     * If we're on a touchscreen, register the input handler.
     */
    if ( piboxGetDisplayType() == PIBOX_LCD )
    {
        piboxLogger(LOG_INFO, "Registering imageTouch.\n");
        piboxTouchRegisterCB(imageTouch, TOUCH_REGION);
        piboxTouchStartProcessor();
    }

    gtk_init(&argc, &argv);

    /* Load db */
    dbLoad();

    if ( isCLIFlagSet( CLI_TEST) )
        gtk_rc_parse(gtkrc);
    else
        piboxLogger(LOG_INFO, "CLI_TEST is not set.\n");
    window = createWindow();
    gtk_window_fullscreen (GTK_WINDOW(window));
    gtk_widget_show_all(window);

    g_timeout_add(250, startKiosk, NULL);
    gtk_main();

    /* Remove display timer */
    if ( image_cycle_id )
        g_source_remove(image_cycle_id);

    if ( isCLIFlagSet(CLI_TOUCH) )
    {
        /* SIGINT is required to exit the touch processor thread. */
        raise(SIGINT);
        piboxTouchShutdownProcessor();
    }

    piboxLoggerShutdown();
    return 0;
}
