/*******************************************************************************
 * pipics
 *
 * db.c:  Functions for managing image files.
 *
 * License: GPL v3
 *
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#define DB_C

/* To avoid deprecation warnings from GTK+ 2.x */
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"

#include <stdio.h>
#include <stdlib.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <glib.h>
#include <pibox/utils.h>
#include <pibox/parson.h>

#include "pipics.h"

/*
 * Linked lists for files.
 */
static GSList   *fileList = NULL;       // List of files to play
static char     *activeFile = NULL;     // Currently active file to display

static char readbuf[PATH_MAX];
static int idx=0;

/*
 * Image formats we support.  gdk-pixbuf supports others
 * but let's just deal with popular formats.
 */
static char *suffices[] = {
    "png",
    "jpg",
    "gif",
    "tiff",
    NULL
};

/*
 *========================================================================
 * Name:   getFiles
 * Prototype:  void getFiles( void )
 *
 * Description:
 * Generate a list of image files.
 *========================================================================
 */
static void 
getFiles ( void )
{
    int     i;
    FILE    *pd = NULL;
    FILE    *fd = NULL;
    char    findCmd[128];
    char    *pathPtr;
    char    *filename;
    char    *suffix;

    if ( isCLIFlagSet( CLI_TEST) )
    {
        sprintf(findCmd, "find -L . -type f");
    }
    else
    {
        sprintf(findCmd, "find . -type f");
    }
    piboxLogger(LOG_INFO, "Find command: %s\n", findCmd);

    // Find all files the current directory tree.
    pd = popen(findCmd, "r");
    if ( pd == NULL )
    {
        piboxLogger(LOG_ERROR, "Can't find image files");
        return;
    }

    // Open an output file
    fd = fopen(PIPICS_DATA_F, "w");
    if ( fd == NULL )
    {
        piboxLogger(LOG_ERROR, "Can't open file to save file list: %s\n", PIPICS_DATA_F);
        return;
    }

    // Read each line
    while( fgets(readbuf, PATH_MAX, pd) != NULL )
    {
        pathPtr = readbuf;
        pathPtr = piboxTrim(pathPtr);
        piboxStripNewline(pathPtr);
        filename = strdup(pathPtr);
        piboxLogger(LOG_INFO, "Filename: %s\n", filename);
        if ( rindex(filename, '.') == NULL )
        {
            free(filename);
            continue;
        }
        suffix = rindex(filename, '.') + 1;
        for(i=0; suffices[i] != NULL; i++)
        {
            if ( strcasecmp(suffix, suffices[i]) == 0 )
                break;
        }
        if ( suffices[i] == NULL )
        {
            free(filename);
            continue;
        }

        piboxLogger(LOG_INFO, "Saveing filename: %s\n", filename);
        fprintf(fd, "%s\n", filename);
        fileList = g_slist_append(fileList, filename);
    }

    pclose(pd);
    fclose(fd);
}

/*
 *========================================================================
 * Name:   dbLoad
 * Prototype:  void dbLoad( void )
 *
 * Description:
 * Load the database into a local link list.
 *
 * Notes:
 * We changed to the top of the db directory tree when we started, so 
 * we just search for files from the current location.
 *========================================================================
 */
void 
dbLoad()
{
    /*
     * Create list of files to play.
     */
    getFiles();
}

/*
 *========================================================================
 * Name:   getNextImage
 * Prototype:  void getNextImage( void )
 *
 * Description:
 * Make the next image the active image.  
 *========================================================================
 */
void
getNextImage( void )
{
    idx++;
    if ( idx > g_slist_length(fileList)-1 )
    {
        /* Just wrap back to the beginning */
        idx = 0;
    }
    activeFile = g_slist_nth_data(fileList, idx);
}

/*
 *========================================================================
 * Name:   getPrevImage
 * Prototype:  void getPrevImage( void )
 *
 * Description:
 * Make the previous image the active image.  
 *========================================================================
 */
void
getPrevImage( void )
{
    idx--;
    if ( idx < 0 )
    {
        /* Just wrap back to the beginning */
        idx = g_slist_length(fileList) - 1;
    }
    activeFile = g_slist_nth_data(fileList, idx);
}

/*
 *========================================================================
 * Name:   getActiveFile
 * Prototype:  void getActiveFile( void )
 *
 * Description:
 * Get the currently active file.
 *========================================================================
 */
char *
getActiveFile( void )
{
    return(activeFile);
}
